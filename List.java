/**
*	List.java : Implementation of modified list module for PA3
*	Name: Kaile Baran
*	Student ID: kbaran
*	Assignment: Programming Assignment 3
*	Course: CMPS 101
*
*/
public class List 
{
	private Node front;
	private Node back;
	private Node cursor;
	private int length;
	private int cursorIndex;
	
	//Default constructor
	//Creates a new empty list.
	public List()
	{
		front = null;
		back = null;
		cursor = null;
		length = 0;
		cursorIndex = -1;
	}
	// Returns the number of elements in this List.
	public int length()
	{
		return length;
	}
	// If cursor is defined, returns the index of the cursor element,
	// otherwise returns -1.
	public int index()
	{
		return cursorIndex;
	}
	// Returns front element. Pre: length()>0
	public Object front()
	{
		if(length() == 0)
		{
			System.out.println("List Error: Calling front() on empty list.");
			System.exit(1);
		}
		return front.data;
	}
	//Returns back element. Pre: length()>0
	public Object back()
	{
		if(length() == 0)
		{
			System.out.println("List Error: Calling back() on empty list.");
			System.exit(1);
		}
		return back.data;
	}
	//Returns cursor element. Pre: length()>0, index()>=0
	public Object get()
	{
		if(length() == 0)
		{
			System.out.println("List Error: Calling get() on empty list.");
			System.exit(1);
		}
		else if(index() < 0)
		{
			System.out.println("List Error: Calling get() on null cursor element");
			System.exit(1);
		}
		return cursor.data;
	}
	//Returns true if and only if this List and L are the same. The states of the cursors
	//in the two Lists are not used in determining equality.
	@Override
	public boolean equals(Object x)
	{
		if(!(x instanceof List))
		{
			return false;
		}			
		List L = (List) x;
		if(this.length() == L.length())
		{
			if(this.length()>0)
			{
				Node a = front;
				Node b = L.front;
				while(a !=null && b !=null)
				{
					if(!a.equals(b))
					{
						return false;
					}
					a=a.next;
					b=b.next;
				}
			}
			return true;
		}
		return false;
	}
	//Resets this List to its original empty state.
	public void clear()
	{
		front = null;
		back = null;
		cursor = null;
		cursorIndex = -1;
		length = 0;
	}
	//If List is non-empty, places the cursor under the front element,
	//otherwise does nothing.
	public void moveFront()
	{
		if(length() > 0)
		{
			cursor = front;
			cursorIndex = 0;
		}
	}
	//If List is non-empty, places the cursor under the back element,
	//otherwise does nothing.
	public void moveBack()
	{
		if(length > 0)
		{
			cursor = back;
			cursorIndex = (length() -1);
		}
	}
	//If cursor is defined and not at front, moves cursor one step toward
	//front of this List, if cursor is defined and at front, cursor becomes null
	public void movePrev()
	{
		if(cursor.equals(front))
		{
			cursor = null;
			cursorIndex = -1;
		}
		else
		{
			cursor = cursor.previous;
			cursorIndex--;
		}
	}
	// If cursor is defined and notat back, moves cursor one step toward 
	// back of this List, if cursor is defined and at back, cursor becomes
	// undefined, if cursor is undefined does nothing.
	public void moveNext()
	{
		if(cursor.equals(back))
		{
			cursor = null;
			cursorIndex = -1;
		}
		else
		{
			cursor = cursor.next;
			cursorIndex++;
		}
	}
	//Insert new element into this List.  If List is non-empty, 
	//insertion takes placebefore front element.
	public void prepend(Object data)
	{
		if(length() == 0)
		{
			front = new Node(data);
			back = front;
			cursor = front;
			cursorIndex = 0;
			length++;
		}
		else if(length() > 0)
		{
			Node temp = new Node(data);
			front.previous = temp;
			temp.next = front;
			front = temp;
			cursor = front;
			cursorIndex = 0;
			length++;
		}
	}
	//Insert new element into this List.  If List is non-empty, 
	//insertion takes place after back element.
	public void append(Object data)
	{
		if(length() == 0)
		{
			front = new Node(data);
			back = front;
			cursor = back;
			cursorIndex = 0;
			length++;
		}
		else if(length() > 0)
		{
			Node temp = new Node(data);
			back.next = temp;
			temp.previous = back;
			back = temp;
			cursor = back;
			cursorIndex = length() -1;
			length++;
		}
			
	}
	//Insert new element before cursor.
	//Pre: length()>0, index()>=0
	public void insertBefore(Object data)
	{
		if(length() > 0 && index() >= 0)
		{
			if(cursor.equals(front))
			{
				prepend(data);				
			}
			else
			{
				Node temp = new Node(data);
				temp.next = cursor;
				temp.previous = cursor.previous;
				cursor.previous.next = temp;
				cursor.previous = temp;
				length++;
			}
		}		
	}
	//Inserts new element after cursor.
	//Pre: length()>0, index()>=0
	public void insertAfter(Object data)
	{
		if(length() > 0 && index() >= 0)
		{
			if(cursor.equals(back))
			{
				append(data);
			}
			else
			{
				Node temp = new Node(data);
				temp.previous = cursor;
				temp.next = cursor.next;
				cursor.next.previous = temp;
				cursor.next = temp;
				length++;
			}
		}
	}
	//Deletes the front element. Pre: length()>0
	public void deleteFront()
	{
		if(length() == 1)
		{
			front = null;
			back = null;
			cursor = null;
			cursorIndex = -1;
			length--;
		}
		else if(length() > 1)
		{
			cursor = front.next;
			cursor.previous = null;
			front = cursor;
			cursorIndex = 0;
			length--;
		}
	}
	//Deletes the back element. Pre: length()>0
	public void deleteBack()
	{
		if(length() == 1)
		{
			front = null;
			back = null;
			cursor = null;
			cursorIndex = -1;
			length--;
		}
		else if(length() > 1)
		{
			cursor = back.previous;
			cursor.next = null;
			back = cursor;
			cursorIndex = length() - 1;
			length--;
		}
	}
	//Deletes cursor element, making cursor undefined.
	//Pre: length()>0, index()>=0
	public void delete()
	{
		if( length() > 0 && index() >=0)
		{
			if(cursor.equals(front))
			{
				deleteFront();
			}
			else if(cursor.equals(back))
			{
				deleteBack();
			}
			else
			{
				Node temp = cursor;
				temp.previous.next = temp.next;
				temp.next = cursor.next;
				cursor = null;
				cursorIndex = -1;
				length--;				
			}
		}
	}
	//Overrides Object's toString method. Returns a String
	//representation of this List consisting of a space 
	//separated sequence of integers, with front on left.
	@Override
	public String toString()
	{
		String temp = "";
		moveFront();
		for(int i = 0; i < length(); i++)
		{			
			temp += get();
			temp += " ";
			moveNext();
		}
		return temp;
	}
	private static class Node
	{
		private Node next;
		private Node previous;
		private Object data;
		/**
		 * Default (no arg) constructor will set data and next values to null. Basically a node with no data
		 * and no node following it or preceding it.
		 * 
		 */
		Node()
		{
			data = null;
			next = null;
			previous = null;
		}
		/**
		 * Additional constructor that allows for only an int data value to be passed in
		 * and no node following it or preceding it.
		 * 
		 */
		Node(Object data)
		{
			this.data = data;
			next = null;
			previous = null;
		}
		/**
		 *Simple constructor that will take in an int and two Node objects as its data and next/previous values.
		 *respectively.
		 *@param data
		 *			Passed in object that the Node will hold
		 *@param next
		 *			The Node that will follow in a List
		 * 
		 */
		Node(Object data, Node next, Node previous)
		{
			this.data = data;
			this.next = next;
			this.previous = previous;
		}
		/**
		 * Override of equals method. Will compare data field.
		 * 
		 * @param elem
		 * 			A passed in Node that will be used for comparison
		 * @return will return true if Nodes hold the same value, false if not.
		 * 
		 */
		 @Override
		public boolean equals(Object elem)
		{
			Node temp = (Node) elem;
			if(this.data.equals(temp.data))
			{
				return true;
			}
			return false;			
		}
		/**
		 * Override of Objects toString method will return the data field of the current node
		 * as well as the data fields of its next and previous Nodes.
		 * 
		 */
		@Override
		public String toString()
		{
			return "Data: " + data.toString() + " Next: " + next.toString() + " Previous: " + previous.toString();
		}
	}

}
