/**
*	Sparse.java : Contains main for PA3 and conducts file IO.
*	Student ID: kbaran
*	Assignment: Programming Assignment 3
*	Course: CMPS 101
*
*/
import java.io.*;
import java.util.*;
public class Sparse
{
	public static void main(String[] args) throws IOException
	{
		String[] inputArray;
		Matrix A;
		Matrix B;
		//checking for proper number of command line inputs.
		if(args.length > 2)
		{
			System.err.println("Too many strings given as input.");
			System.exit(1);
		}
		else if(args.length < 2)
		{
			System.err.println("Too few strings given as input.");
			System.exit(2);
		}
		try
		{
			int n = 0;
			
			Scanner fileInput = new Scanner(new File(args[0]));
			//counting lines in input file
			while(fileInput.hasNextLine())
			{
				n++;
				fileInput.nextLine();
			}
			fileInput.close();
			
			int count = 0;
			inputArray = new String[n];
			fileInput = new Scanner(new File(args[0]));		
			//reading lines from file
			while(fileInput.hasNextLine())
			{
				inputArray[count] = fileInput.nextLine();
				count++;
			}
			fileInput.close();
			//finding dimensions of matrices from input file
			String[] dimensions = inputArray[0].split("\\s");
			A = new Matrix(Integer.parseInt(dimensions[0]));
			B = new Matrix(Integer.parseInt(dimensions[0]));
			String[] line;
			//building A matrix
			for(int i = 2; i <= Integer.parseInt(dimensions[1]) +1; i++)
			{
				line = inputArray[i].split("\\s");
				A.changeEntry(Integer.parseInt(line[0]),Integer.parseInt(line[1]),Double.parseDouble(line[2]));
			}
			//building B matrix
			int num = 3+Integer.parseInt(dimensions[1]);
			for(int i = num; i < num + Integer.parseInt(dimensions[2]); i++)
			{
				line = inputArray[i].split("\\s");
				B.changeEntry(Integer.parseInt(line[0]),Integer.parseInt(line[1]),Double.parseDouble(line[2]));
			}
			
			PrintWriter pw = new PrintWriter(new FileWriter(args[1]));
			pw.println("A has " + A.getNNZ() + " non-zero entries:");
			pw.print(A);
			
			pw.println("\nB has " + B.getNNZ() + " non-zero entries:");
			pw.print(B);
			

			pw.println("\n1.5*A =");
			pw.print(A.scalarMult(1.5));
			

			pw.println("\nA+B =");
			pw.print(A.add(B));
			

			pw.println("\nA+A =");
			pw.print(A.add(A));
			
			pw.println("\nB-A =");
			pw.print(B.sub(A));
			
			pw.println("\nA-A =");
			pw.print(A.sub(A));
			
			pw.println("\nTranspose(A) =");
			pw.print(A.transpose());
			
			pw.println("\nA*B =");
			pw.print(A.mult(B));
			
			pw.println("\nB*B =");
			pw.print(B.mult(B));
			
			pw.close();
		}
		catch(Exception e)
		{
			System.out.println(e);
		}
	}
}