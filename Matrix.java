/**
*	Matrix.java : Implementation of sparse matrix module for PA3
*	Name: Kaile Baran
*	Student ID: kbaran
*	Assignment: Programming Assignment 3
*	Course: CMPS 101
*
*/
public class Matrix
{
	private List[] matrix;
	private int size;
	private int nnz;
	
	//Constructor
	// Makes a new n x n zero Matrix.  pre: n>=1
	public Matrix(int n)
	{
		size = n;
		if(size<1)
		{
			throw new RuntimeException("Invalid Matrix dimension given.");
		}
		matrix = new List[getSize()];
		for(int i = 0; i < getSize(); i++)
		{
			matrix[i] = new List();
		}
		nnz = 0;
	}
	//Access functions
	// Returns n, the number of rows and columns of this Matrix
	public int getSize()
	{
		return size;
	}
	// Returns the number of non-zero entries in this Matrix
	public int getNNZ()
	{
		for(int i = 0; i<getSize(); i++)
		{
			nnz+= matrix[i].length();
		}
		return nnz;
	}
	//Overrides Object's equals() method
	@Override
	public boolean equals(Object x)
	{
		if(x instanceof Matrix)
		{
			Matrix temp = (Matrix) x;
			if(getSize() == temp.getSize())
			{
				for(int i = 0; i < getSize(); i++)
				{
					if(!(matrix[i].equals(temp.matrix[i])))
					{
						return false;
					}				
				}
				return true;
			}			
		}
		return false;
	}
	//Manipulation procedures
	//Sets this Matrix to the zero state
	public void makeZero()
	{
		matrix = new List[getSize()];
			for(int i = 0; i < getSize(); i++)
		{
			matrix[i] = new List();
		}
	}
	//Returns a new Matrix having the same entries as this Matrix
	public Matrix copy()
	{
		Matrix copy = new Matrix(getSize());
		for(int i = 0; i < getSize(); i++)
		{
			copy.matrix[i] = this.matrix[i];
		}
		return copy;
	}
	// changes ith row, jth column of this Matrix to x
	// pre: 1<=i<=getSize(), 1<=j<=getSize()
	public void changeEntry(int i, int j, double x)
	{
		if(!(1<=i && i<=getSize()) || !(1<=j && j<=getSize()))
		{
			throw new RuntimeException("Invalid coordinates for changeEntry().");
		}
		if(matrix[i-1].length()==0)
		{
			if(x !=0)
			{
				Object temp = new Entry(j,x);
				matrix[i-1].append(temp);
			}
		}
		else if(x != 0)
		{
			boolean added = false;
			matrix[i-1].moveFront();
			Entry current;
			for(int k = 0; k < matrix[i-1].length(); k++)
			{
				current = (Entry) matrix[i-1].get();
				if(current.column == j)
				{
					current.value = x;
					added = true;
				}
				matrix[i-1].moveNext();
			}
			if(!added)
			{
				matrix[i-1].append(new Entry(j,x));
			}
		}
	}
	// returns a new Matrix that is the scalar product of this Matrix with x
	public Matrix scalarMult(double x)
	{
		Matrix temp = new Matrix(getSize());

		if(x != 0.0)
		{
			Entry ent;
			for(int i = 0; i < getSize(); i++)
			{
				temp.matrix[i] = scalarMultHelper(this.matrix[i], x);
			}
		}
		return temp;
	}
	//helper method for scalarMult()
	private List scalarMultHelper(List l, double val)
	{
		List temp = new List();
		if(l.length() > 0)
		{
			l.moveFront();
			while(l.index() != -1)
			{				
				temp.append(new Entry(((Entry)l.get()).column, (((Entry)l.get()).value)*val));
				l.moveNext();
			}
		}
		return temp;
	}
	// Returns a new Matrix that is the sum of this Matrix with M
	// pre: getSize()==M.getSize()	
	public Matrix add(Matrix M)
	{
		Matrix temp = new Matrix(getSize());
		if(getSize() != M.getSize())
		{
			throw new RuntimeException("Size difference between matrices in add() method");
		}
		else if(this.equals(M))
		{
			return scalarMult(2.0);
		}
		for(int i = 0; i < getSize(); i++)
		{
			temp.matrix[i] = addHelper(this.matrix[i], M.matrix[i]);
		}
		return temp;
		
	}
	//helper method for add() will return the sum of entries from two lists
	private List addHelper(List list1, List list2)
	{
		List temp = new List();
		if(list1.length() == 0 && list2.length() == 0)
		{
			return temp;
		}
		else if(list1.length() == 0 && list2.length() != 0)
		{
			temp = list2;
		}
		else if(list2.length() == 0 && list1.length() !=0)
		{
			temp = list1;
		}
		else
		{
			Entry tempEntry1;
			Entry tempEntry2;
			list1.moveFront();
			list2.moveFront();
			while(list1.index() != -1 && list2.index() != -1)
			{
				tempEntry1 = (Entry)list1.get();
				tempEntry2 = (Entry)list2.get();
				if(tempEntry1.column == tempEntry2.column)
				{
					if((tempEntry1.value + tempEntry2.value) != 0)
					{
						temp.append(new Entry(tempEntry1.column, (tempEntry1.value + tempEntry2.value)));
					}
					list1.moveNext();
					list2.moveNext();
				}
				else if(tempEntry1.column > tempEntry2.column)
				{
					temp.append(new Entry(tempEntry2.column, tempEntry2.value));
					list2.moveNext();
				}
				else if(tempEntry1.column < tempEntry2.column)
				{
					temp.append(new Entry(tempEntry1.column, tempEntry1.value));
					list1.moveNext();
				}
			}
			while(list1.index() != -1)
			{
				tempEntry1 = (Entry)list1.get();
				temp.append(new Entry(tempEntry1.column, tempEntry1.value));
				list1.moveNext();
			}
			while(list2.index() != -1)
			{
				tempEntry2 = (Entry)list2.get();
				temp.append(new Entry(tempEntry2.column, tempEntry2.value));
				list2.moveNext();
			}
		}
		return temp;
	}
	// returns a new Matrix that is the difference of this Matrix with M
	// pre: getSize()==M.getSize()
	public Matrix sub(Matrix M)
	{
		Matrix temp = new Matrix(getSize());
		if(getSize() != M.getSize())
		{
			throw new RuntimeException("Size difference between matrices in sub() method");
		}
		else if(this.equals(M))
		{
			return temp;
		}
		for(int i = 0; i < getSize(); i++)
		{
			temp.matrix[i] = subHelper(this.matrix[i], M.matrix[i]);
		}
		return temp;
	}
	//helper method for sub() will return the difference between two lists
	private List subHelper(List list1, List list2)
	{		
		List temp = new List();
		if(list1.length() == 0 && list2.length() == 0)
		{
			return temp;
		}
		else if(list1.length() == 0 && list2.length() != 0)
		{
			list2.moveFront();
			while(list2.index() != -1)
			{
				temp.append(new Entry(((Entry)list2.get()).column, ((Entry)list2.get()).value * -1));
				list2.moveNext();
			}
		}
		else if(list2.length() == 0 && list1.length() !=0)
		{
			temp = list1;
		}
		else
		{
			Entry tempEntry1;
			Entry tempEntry2;
			list1.moveFront();
			list2.moveFront();
			while(list1.index() != -1 && list2.index() != -1)
			{
				tempEntry1 = (Entry)list1.get();
				tempEntry2 = (Entry)list2.get();
				if(tempEntry1.column == tempEntry2.column)
				{
					if((tempEntry1.value - tempEntry2.value) != 0)
					{
						temp.append(new Entry(tempEntry1.column, (tempEntry1.value - tempEntry2.value)));
					}
					list1.moveNext();
					list2.moveNext();
				}
				else if(tempEntry1.column > tempEntry2.column)
				{
					temp.append(new Entry(tempEntry2.column, -1*tempEntry2.value));
					list2.moveNext();
				}
				else if(tempEntry1.column < tempEntry2.column)
				{
					temp.append(new Entry(tempEntry1.column, tempEntry1.value));
					list1.moveNext();
				}
			}
			while(list1.index() != -1)
			{
				tempEntry1 = (Entry)list1.get();
				temp.append(new Entry(tempEntry1.column, tempEntry1.value));
				list1.moveNext();
			}
			while(list2.index() != -1)
			{
				tempEntry2 = (Entry)list2.get();
				temp.append(new Entry(tempEntry2.column, tempEntry2.value));
				list2.moveNext();
			}
		}
		return temp;
	}		
	// returns a new Matrix that is the transpose of this Matrix
	public Matrix transpose()
	{
		Matrix temp = new Matrix(getSize());
		for(int i = 0; i < getSize(); i++)
		{
			matrix[i].moveFront();
			
			while(matrix[i].index() != -1)
			{
				temp.changeEntry(((Entry)matrix[i].get()).column, i+1 , ((Entry)matrix[i].get()).value);
				matrix[i].moveNext();
			}
		}
		return temp;
	}
	// returns a new Matrix that is the product of this Matrix with M
	// pre: getSize()==M.getSize()
	public Matrix mult(Matrix M)
	{
		if(getSize() != M.getSize())
		{
			throw new RuntimeException("Size differences between matrices in mult() method");
		}
		Matrix temp = M.transpose();
		Matrix product = new Matrix(getSize());
		for(int i = 0; i < getSize(); i++)
		{
			for(int j = 0; j < getSize(); j++)
			{
				product.changeEntry(i+1,j+1, dot(matrix[i],temp.matrix[j]));
			}
		}
		return product;	
	}
	//helper method for mult() will return the dot product of two vectors
	private double dot(List list1, List list2)
	{		
		double dotP = 0.0;
		if(list1.length() == 0 || list2.length() == 0)
		{
			return dotP;
		}
		else
		{
			list1.moveFront();
			list2.moveFront();
			double temp1 = 0;
			double temp2 = 0;
			while(list1.index() != -1 && list2.index() != -1)
			{
				if(((Entry)list1.get()).column == ((Entry)list2.get()).column)
				{
					temp1 = ((Entry)list1.get()).value;
					temp2 = ((Entry)list2.get()).value;				
					dotP += (temp1*temp2);
					list1.moveNext();
					list2.moveNext();
				}
				else if(((Entry)list1.get()).column < ((Entry)list2.get()).column)
				{
					list1.moveNext();
				}
				else if(((Entry)list1.get()).column > ((Entry)list2.get()).column)
				{
					list2.moveNext();
				}				
			}
		}
		return dotP;
	}
	//Overrides Object's toString() method
	@Override
	public String toString()
	{
		String s = "";
		for(int i = 0; i < getSize(); i++)
		{
			matrix[i].moveFront();
			Entry temp;
			if(matrix[i].length() > 0)
			{
				s+= i+1 + ": ";
			}			
			for(int j = 0; j < matrix[i].length(); j++)
			{
				temp = (Entry) matrix[i].get();
				s+= temp.toString() + " ";
				matrix[i].moveNext();
			}
			if(matrix[i].length()>0)
			{
				s+="\n";
			}
			
		}
		return s;		
	}	
	//private nested class contains Objects that will be used to
	//to hold data for Matrix elements.
	private static class Entry
	{
		private int column;
		private double value;
		Entry()
		{
			this.column = 0;
			this.value = 0.0;
		}
		Entry(int column, double value)
		{
			this.column = column;
			this.value = value;
		}
		@Override
		public boolean equals(Object o)
		{
			if(o instanceof Entry)
			{
				Entry temp = (Entry) o;
				if(temp.column == this.column && temp.value == this.value)
				{
					return true;
				}
			}
			return false;
		}
		@Override
		public String toString()
		{
			return "(" + column + ", " + value + ")";
		}
	}
}
	