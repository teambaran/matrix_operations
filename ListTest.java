/**
*	ListTest.java : Contains main for testing the accuracy of all public methods of List.java
*	Name: Kaile Baran
*	Student ID: kbaran
*	Assignment: Programming Assignment 3
*	Course: CMPS 101
*
*/
public class ListTest
{
	public static void main(String[] args)
	{
		List list1 = new List();
		List list2 = new List();
		
		list1.append(5);
		list1.append(8);
		list1.append(12);
		list1.append(6);		

		list2.prepend(8);
		list2.prepend(5);

		
		
		System.out.println("list1: " + list1);
		System.out.println("list2: " + list2);

		
		System.out.println("\nSize of list1: " + list1.length());
		list1.moveFront();
		System.out.println("First element of list1: " + list1.get());
		System.out.println("list2.front(): " + list2.front());
		list1.moveBack();
		System.out.println("Last element of list1: " + list1.get());
		System.out.println("list2.back(): " + list2.back());
		
		System.out.println("list1 == list2? " + list1.equals(list2));
		list2.append(12);
		list2.append(6);
		
		System.out.println("list1 == list2? " + list1.equals(list2));
		
		list1.moveFront();
		while(list1.index() != -1)
		{
			System.out.print(list1.get() + " ");
			list1.moveNext();
		}
		System.out.println("\n");
		list1.moveBack();
		list1.movePrev();
		list1.delete();
		System.out.println(list1);
		list1.append(15);
		list1.append(23);
		
		System.out.println(list1);
		list1.deleteFront();
		list1.deleteBack();
		System.out.println(list1);
		list1.moveFront();
		list1.insertBefore(7);
		list1.moveNext();
		list1.insertAfter(9);
		System.out.println(list1);
		list1.clear();
		System.out.println(list1);
		System.out.println("list1 size: " + list1.length());
		
		
		
	}
}